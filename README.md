# Blackjack

This is a C# Blackjack game with a GUI. However, it is still in development.

### TODO:
1. Create method to determine when the computer will "hit"
   * Should not hit if hand score is 17 or higher
2. Determine how to implement Ace's score of 1 or 11
   * Use a counter to keep track of Aces in hand (DONE)
   * If counter is greater than 0 and hand busts subtract 10 points from score, decrement counter, and check again
   * Create global variable to keep track of on-going score
3. Figure out how to keep an on-going score for both user and computer hands without them changing the score if it has been changed previously due to an Ace busting the hand
   * This should be solved by the Ace counter variable