﻿namespace Blackjack
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.cCard1PictureBox = new System.Windows.Forms.PictureBox();
            this.cCard2PictureBox = new System.Windows.Forms.PictureBox();
            this.cCard3PictureBox = new System.Windows.Forms.PictureBox();
            this.cCard4PictureBox = new System.Windows.Forms.PictureBox();
            this.cCard5PictureBox = new System.Windows.Forms.PictureBox();
            this.uCard1PictureBox = new System.Windows.Forms.PictureBox();
            this.uCard2PictureBox = new System.Windows.Forms.PictureBox();
            this.uCard3PictureBox = new System.Windows.Forms.PictureBox();
            this.uCard4PictureBox = new System.Windows.Forms.PictureBox();
            this.uCard5PictureBox = new System.Windows.Forms.PictureBox();
            this.newGameButton = new System.Windows.Forms.Button();
            this.hit1Button = new System.Windows.Forms.Button();
            this.standButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.standToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howToPlayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cardImageList = new System.Windows.Forms.ImageList(this.components);
            this.hit2Button = new System.Windows.Forms.Button();
            this.hit3Button = new System.Windows.Forms.Button();
            this.computerScoreLabel = new System.Windows.Forms.Label();
            this.userScoreLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.cCard1PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCard2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCard3PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCard4PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCard5PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCard1PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCard2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCard3PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCard4PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCard5PictureBox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cCard1PictureBox
            // 
            this.cCard1PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.cCard1PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cCard1PictureBox.Location = new System.Drawing.Point(71, 51);
            this.cCard1PictureBox.Name = "cCard1PictureBox";
            this.cCard1PictureBox.Size = new System.Drawing.Size(134, 187);
            this.cCard1PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cCard1PictureBox.TabIndex = 0;
            this.cCard1PictureBox.TabStop = false;
            // 
            // cCard2PictureBox
            // 
            this.cCard2PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.cCard2PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cCard2PictureBox.Location = new System.Drawing.Point(113, 51);
            this.cCard2PictureBox.Name = "cCard2PictureBox";
            this.cCard2PictureBox.Size = new System.Drawing.Size(134, 187);
            this.cCard2PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cCard2PictureBox.TabIndex = 1;
            this.cCard2PictureBox.TabStop = false;
            // 
            // cCard3PictureBox
            // 
            this.cCard3PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.cCard3PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cCard3PictureBox.Location = new System.Drawing.Point(316, 51);
            this.cCard3PictureBox.Name = "cCard3PictureBox";
            this.cCard3PictureBox.Size = new System.Drawing.Size(134, 187);
            this.cCard3PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cCard3PictureBox.TabIndex = 2;
            this.cCard3PictureBox.TabStop = false;
            // 
            // cCard4PictureBox
            // 
            this.cCard4PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.cCard4PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cCard4PictureBox.Location = new System.Drawing.Point(485, 51);
            this.cCard4PictureBox.Name = "cCard4PictureBox";
            this.cCard4PictureBox.Size = new System.Drawing.Size(134, 187);
            this.cCard4PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cCard4PictureBox.TabIndex = 3;
            this.cCard4PictureBox.TabStop = false;
            // 
            // cCard5PictureBox
            // 
            this.cCard5PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.cCard5PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cCard5PictureBox.Location = new System.Drawing.Point(653, 51);
            this.cCard5PictureBox.Name = "cCard5PictureBox";
            this.cCard5PictureBox.Size = new System.Drawing.Size(134, 187);
            this.cCard5PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cCard5PictureBox.TabIndex = 4;
            this.cCard5PictureBox.TabStop = false;
            // 
            // uCard1PictureBox
            // 
            this.uCard1PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.uCard1PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uCard1PictureBox.Location = new System.Drawing.Point(71, 330);
            this.uCard1PictureBox.Name = "uCard1PictureBox";
            this.uCard1PictureBox.Size = new System.Drawing.Size(134, 187);
            this.uCard1PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.uCard1PictureBox.TabIndex = 5;
            this.uCard1PictureBox.TabStop = false;
            // 
            // uCard2PictureBox
            // 
            this.uCard2PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.uCard2PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uCard2PictureBox.Location = new System.Drawing.Point(113, 330);
            this.uCard2PictureBox.Name = "uCard2PictureBox";
            this.uCard2PictureBox.Size = new System.Drawing.Size(134, 187);
            this.uCard2PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.uCard2PictureBox.TabIndex = 6;
            this.uCard2PictureBox.TabStop = false;
            // 
            // uCard3PictureBox
            // 
            this.uCard3PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.uCard3PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uCard3PictureBox.Location = new System.Drawing.Point(316, 330);
            this.uCard3PictureBox.Name = "uCard3PictureBox";
            this.uCard3PictureBox.Size = new System.Drawing.Size(134, 187);
            this.uCard3PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.uCard3PictureBox.TabIndex = 7;
            this.uCard3PictureBox.TabStop = false;
            // 
            // uCard4PictureBox
            // 
            this.uCard4PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.uCard4PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uCard4PictureBox.Location = new System.Drawing.Point(485, 330);
            this.uCard4PictureBox.Name = "uCard4PictureBox";
            this.uCard4PictureBox.Size = new System.Drawing.Size(134, 187);
            this.uCard4PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.uCard4PictureBox.TabIndex = 8;
            this.uCard4PictureBox.TabStop = false;
            // 
            // uCard5PictureBox
            // 
            this.uCard5PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.uCard5PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.uCard5PictureBox.Location = new System.Drawing.Point(653, 330);
            this.uCard5PictureBox.Name = "uCard5PictureBox";
            this.uCard5PictureBox.Size = new System.Drawing.Size(134, 187);
            this.uCard5PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.uCard5PictureBox.TabIndex = 9;
            this.uCard5PictureBox.TabStop = false;
            // 
            // newGameButton
            // 
            this.newGameButton.Location = new System.Drawing.Point(940, 249);
            this.newGameButton.Name = "newGameButton";
            this.newGameButton.Size = new System.Drawing.Size(75, 23);
            this.newGameButton.TabIndex = 10;
            this.newGameButton.Text = "New Game";
            this.newGameButton.UseVisualStyleBackColor = true;
            this.newGameButton.Click += new System.EventHandler(this.newGameButton_Click);
            // 
            // hit1Button
            // 
            this.hit1Button.Location = new System.Drawing.Point(940, 278);
            this.hit1Button.Name = "hit1Button";
            this.hit1Button.Size = new System.Drawing.Size(75, 23);
            this.hit1Button.TabIndex = 11;
            this.hit1Button.Text = "Hit";
            this.hit1Button.UseVisualStyleBackColor = true;
            this.hit1Button.Click += new System.EventHandler(this.hit1Button_Click);
            // 
            // standButton
            // 
            this.standButton.Location = new System.Drawing.Point(940, 307);
            this.standButton.Name = "standButton";
            this.standButton.Size = new System.Drawing.Size(75, 23);
            this.standButton.TabIndex = 12;
            this.standButton.Text = "Stand";
            this.standButton.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.gameToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1072, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newGameToolStripMenuItem
            // 
            this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
            this.newGameToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.newGameToolStripMenuItem.Text = "New Game";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // gameToolStripMenuItem
            // 
            this.gameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hitToolStripMenuItem,
            this.standToolStripMenuItem});
            this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
            this.gameToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.gameToolStripMenuItem.Text = "Game";
            // 
            // hitToolStripMenuItem
            // 
            this.hitToolStripMenuItem.Name = "hitToolStripMenuItem";
            this.hitToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.hitToolStripMenuItem.Text = "Hit";
            // 
            // standToolStripMenuItem
            // 
            this.standToolStripMenuItem.Name = "standToolStripMenuItem";
            this.standToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.standToolStripMenuItem.Text = "Stand";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.howToPlayToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // howToPlayToolStripMenuItem
            // 
            this.howToPlayToolStripMenuItem.Name = "howToPlayToolStripMenuItem";
            this.howToPlayToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.howToPlayToolStripMenuItem.Text = "About";
            // 
            // cardImageList
            // 
            this.cardImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("cardImageList.ImageStream")));
            this.cardImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.cardImageList.Images.SetKeyName(0, "2_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(1, "2_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(2, "2_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(3, "2_Spades.jpg");
            this.cardImageList.Images.SetKeyName(4, "3_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(5, "3_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(6, "3_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(7, "3_Spades.jpg");
            this.cardImageList.Images.SetKeyName(8, "4_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(9, "4_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(10, "4_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(11, "4_Spades.jpg");
            this.cardImageList.Images.SetKeyName(12, "5_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(13, "5_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(14, "5_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(15, "5_Spades.jpg");
            this.cardImageList.Images.SetKeyName(16, "6_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(17, "6_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(18, "6_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(19, "6_Spades.jpg");
            this.cardImageList.Images.SetKeyName(20, "7_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(21, "7_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(22, "7_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(23, "7_Spades.jpg");
            this.cardImageList.Images.SetKeyName(24, "8_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(25, "8_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(26, "8_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(27, "8_Spades.jpg");
            this.cardImageList.Images.SetKeyName(28, "9_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(29, "9_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(30, "9_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(31, "9_Spades.jpg");
            this.cardImageList.Images.SetKeyName(32, "10_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(33, "10_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(34, "10_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(35, "10_Spades.jpg");
            this.cardImageList.Images.SetKeyName(36, "Jack_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(37, "Jack_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(38, "Jack_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(39, "Jack_Spades.jpg");
            this.cardImageList.Images.SetKeyName(40, "Queen_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(41, "Queen_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(42, "Queen_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(43, "Queen_Spades.jpg");
            this.cardImageList.Images.SetKeyName(44, "King_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(45, "King_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(46, "King_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(47, "King_Spades.jpg");
            this.cardImageList.Images.SetKeyName(48, "Ace_Clubs.jpg");
            this.cardImageList.Images.SetKeyName(49, "Ace_Diamonds.jpg");
            this.cardImageList.Images.SetKeyName(50, "Ace_Hearts.jpg");
            this.cardImageList.Images.SetKeyName(51, "Ace_Spades.jpg");
            // 
            // hit2Button
            // 
            this.hit2Button.Location = new System.Drawing.Point(940, 278);
            this.hit2Button.Name = "hit2Button";
            this.hit2Button.Size = new System.Drawing.Size(75, 23);
            this.hit2Button.TabIndex = 14;
            this.hit2Button.Text = "Hit";
            this.hit2Button.UseVisualStyleBackColor = true;
            this.hit2Button.Visible = false;
            this.hit2Button.Click += new System.EventHandler(this.hit2Button_Click);
            // 
            // hit3Button
            // 
            this.hit3Button.Location = new System.Drawing.Point(940, 278);
            this.hit3Button.Name = "hit3Button";
            this.hit3Button.Size = new System.Drawing.Size(75, 23);
            this.hit3Button.TabIndex = 15;
            this.hit3Button.Text = "Hit";
            this.hit3Button.UseVisualStyleBackColor = true;
            this.hit3Button.Visible = false;
            this.hit3Button.Click += new System.EventHandler(this.hit3Button_Click);
            // 
            // computerScoreLabel
            // 
            this.computerScoreLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.computerScoreLabel.Location = new System.Drawing.Point(813, 252);
            this.computerScoreLabel.Name = "computerScoreLabel";
            this.computerScoreLabel.Size = new System.Drawing.Size(82, 20);
            this.computerScoreLabel.TabIndex = 16;
            this.computerScoreLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // userScoreLabel
            // 
            this.userScoreLabel.Location = new System.Drawing.Point(813, 306);
            this.userScoreLabel.Name = "userScoreLabel";
            this.userScoreLabel.Size = new System.Drawing.Size(82, 20);
            this.userScoreLabel.TabIndex = 17;
            this.userScoreLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Blackjack.Properties.Resources.Blackjack_Table;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1072, 554);
            this.Controls.Add(this.userScoreLabel);
            this.Controls.Add(this.computerScoreLabel);
            this.Controls.Add(this.hit3Button);
            this.Controls.Add(this.hit2Button);
            this.Controls.Add(this.standButton);
            this.Controls.Add(this.hit1Button);
            this.Controls.Add(this.newGameButton);
            this.Controls.Add(this.uCard5PictureBox);
            this.Controls.Add(this.uCard4PictureBox);
            this.Controls.Add(this.uCard3PictureBox);
            this.Controls.Add(this.uCard2PictureBox);
            this.Controls.Add(this.uCard1PictureBox);
            this.Controls.Add(this.cCard5PictureBox);
            this.Controls.Add(this.cCard4PictureBox);
            this.Controls.Add(this.cCard3PictureBox);
            this.Controls.Add(this.cCard2PictureBox);
            this.Controls.Add(this.cCard1PictureBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Blackjack";
            ((System.ComponentModel.ISupportInitialize)(this.cCard1PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCard2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCard3PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCard4PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cCard5PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCard1PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCard2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCard3PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCard4PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCard5PictureBox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox cCard1PictureBox;
        private System.Windows.Forms.PictureBox cCard2PictureBox;
        private System.Windows.Forms.PictureBox cCard3PictureBox;
        private System.Windows.Forms.PictureBox cCard4PictureBox;
        private System.Windows.Forms.PictureBox cCard5PictureBox;
        private System.Windows.Forms.PictureBox uCard1PictureBox;
        private System.Windows.Forms.PictureBox uCard2PictureBox;
        private System.Windows.Forms.PictureBox uCard3PictureBox;
        private System.Windows.Forms.PictureBox uCard4PictureBox;
        private System.Windows.Forms.PictureBox uCard5PictureBox;
        private System.Windows.Forms.Button newGameButton;
        private System.Windows.Forms.Button hit1Button;
        private System.Windows.Forms.Button standButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howToPlayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem standToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ImageList cardImageList;
        private System.Windows.Forms.Button hit2Button;
        private System.Windows.Forms.Button hit3Button;
        private System.Windows.Forms.Label computerScoreLabel;
        private System.Windows.Forms.Label userScoreLabel;
    }
}

