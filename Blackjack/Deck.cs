﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Blackjack
{
    class Deck
    {
        // ArrayList for a deck of cards
        private ArrayList deck = new ArrayList();

        public Deck()
        {
            BuildDeck(deck);
        }

        public static ArrayList BuildDeck(ArrayList deck)
        {
            // Build deck
            for (int i = 0; i < 52; i++)
            {
                deck.Add(i);
            }

            // Return deck
            return deck;
        }

        public int DeckSize()
        {
            // Local Variable
            int size = deck.Count;

            // Return current size of deck
            return size;
        }

        public int DrawCard()
        {
            // Create a new random object
            Random rand = new Random();

            // Get random card
            int card = (int)deck[rand.Next(DeckSize())];

            // Remove card from deck
            deck.Remove(card);

            // Return the card
            return card;
        }

        public int DrawCard(Random rand)
        {
            // Get random card
            int card = (int)deck[rand.Next(DeckSize())];

            // Remove card from deck
            deck.Remove(card);

            // Return the card
            return card;
        }
    }
}