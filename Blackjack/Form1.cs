﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace Blackjack
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        // Global Variables
        Deck d = new Deck();        // New deck
        int hit;                    // Counter for hit
        int[] cHand = new int[5];   // Computers hand
        int[] uHand = new int[5];   // Users hand
        int compAces = 0;           // Number of aces in computers hand
        int userAces = 0;           // Number of aces in users hand
        int compScore = 0;          // Computers score
        int userScore = 0;          // Users score

        public int Score(int card)
        {
            // Variable to store the score of the hand
            int score = 0;

                switch (card)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                        score += 2;
                        break;
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        score += 3;
                        break;
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                        score += 4;
                        break;
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                        score += 5;
                        break;
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                        score += 6;
                        break;
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                        score += 7;
                        break;
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                        score += 8;
                        break;
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                        score += 9;
                        break;
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                        score += 10;
                        break;
                    case 48:
                    case 49:
                    case 50:
                    case 51:
                        score += 11;
                        break;
                }

            // Return the score
            return score;
        }

        public int ConvertUserAce(int score)
        {
            // Change Ace value to 1 if hand busts
            while (userAces > 0)
            {
                if (score > 21)
                {
                    score -= 10;
                    userAces -= 1;
                }
                else
                {
                    break;
                }
            }

            return score;
        }

        public int ConvertCompAce(int score)
        {
            // Change Ace value to 1 if hand busts
            while(compAces > 0)
            {
                if (score > 21)
                {
                    score -= 10;
                    compAces -= 1;
                }
                else
                {
                    break;
                }
            }

            return score;
        }

        private void newGameButton_Click(object sender, EventArgs e)
        {
            // Reset deck
            d = new Deck();

            // Reset hit count
            hit = 1;

            // Reset score
            userScore = 0;
            compScore = 0;

            // Reset hit button
            hit3Button.Visible = false;
            hit2Button.Visible = false;
            hit1Button.Visible = true;

            // Clear last hand cards
            uCard3PictureBox.Image = null;
            uCard4PictureBox.Image = null;
            uCard5PictureBox.Image = null;
            cCard3PictureBox.Image = null;
            cCard4PictureBox.Image = null;
            cCard5PictureBox.Image = null;

            // Reset Aces in hand count
            userAces = 0;
            compAces = 0;

            // Random number variable
            Random rand = new Random();

            // Draw first two cards for computer and user
            uHand[0] = d.DrawCard(rand);
            cHand[0] = d.DrawCard(rand);
            uHand[1] = d.DrawCard(rand);
            cHand[1] = d.DrawCard(rand);

            // Count Aces in hand
            if (uHand[0] > 47 && uHand[0] < 52)
            {
                userAces++;
            }
            if (uHand[1] > 47 && uHand[1] < 52)
            {
                userAces++;
            }
            if (cHand[0] > 47 && cHand[0] < 52)
            {
                compAces++;
            }
            if (cHand[1] > 47 && cHand[1] < 52)
            {
                compAces++;
            }

            // Display cards on form
            cCard1PictureBox.Image = cardImageList.Images[cHand[0]];
            cCard2PictureBox.Image = cardImageList.Images[cHand[1]];
            uCard1PictureBox.Image = cardImageList.Images[uHand[0]];
            uCard2PictureBox.Image = cardImageList.Images[uHand[1]];

            // Calculate and display score of users hand
            userScore += Score(uHand[0]);
            userScore += Score(uHand[1]);
            int verifiedUserScore = ConvertUserAce(userScore);
            if (verifiedUserScore < userScore)
            {
                userScore = verifiedUserScore;
            }
            userScoreLabel.Text = userScore.ToString();

            // Calculate and display score ofcomputers hand
            compScore += Score(cHand[0]);
            compScore += Score(cHand[1]);
            int verifiedCompScore = ConvertCompAce(compScore);
            if (verifiedCompScore < compScore)
            {
                compScore = verifiedCompScore;
            }
            computerScoreLabel.Text = compScore.ToString();
        }

        private void hit1Button_Click(object sender, EventArgs e)
        {
            if (hit == 1)
            {
                // Draw users third card
                uHand[2] = d.DrawCard();

                // Count Aces in hand
                if (uHand[2] > 47 && uHand[2] < 52)
                {
                    userAces++;
                }

                // Display the card that was drawn
                uCard3PictureBox.Image = cardImageList.Images[uHand[2]];

                // Calculate and display score of users hand
                userScore += Score(uHand[2]);
                int verifiedUserScore = ConvertUserAce(userScore);
                if (verifiedUserScore < userScore)
                {
                    userScore = verifiedUserScore;
                }
                userScoreLabel.Text = userScore.ToString();

                if (compScore < 17)
                {
                    // Draw computers third card
                    cHand[2] = d.DrawCard();

                    // Count Aces in hand
                    if (cHand[2] > 47 && cHand[2] < 52)
                    {
                        compAces++;
                    }

                    // Display the card that was drawn
                    cCard3PictureBox.Image = cardImageList.Images[cHand[2]];

                    // Calculate and display score of computers hand
                    compScore += Score(cHand[2]);
                    int verifiedCompScore = ConvertCompAce(compScore);
                    if (verifiedCompScore < compScore)
                    {
                        compScore = verifiedCompScore;
                    }
                    computerScoreLabel.Text = compScore.ToString();
                }

                // Increase hit number
                hit++;

                // Select next hit button
                hit1Button.Visible = false;
                hit2Button.Visible = true;
            }
        }

        private void hit2Button_Click(object sender, EventArgs e)
        {
            if (hit == 2)
            {
                // Draw users fourth card
                uHand[3] = d.DrawCard();

                // Count Aces in hand
                if (uHand[3] > 47 && uHand[3] < 52)
                {
                    userAces++;
                }

                // Display the card that was drawn
                uCard4PictureBox.Image = cardImageList.Images[uHand[3]];

                // Calculate and display score of users hand
                userScore += Score(uHand[3]);
                int verifiedUserScore = ConvertUserAce(userScore);
                if (verifiedUserScore < userScore)
                {
                    userScore = verifiedUserScore;
                }
                userScoreLabel.Text = userScore.ToString();

                if (compScore < 17)
                {
                    // Draw computers fourth card
                    cHand[3] = d.DrawCard();

                    // Count Aces in hand
                    if (cHand[3] > 47 && cHand[3] < 52)
                    {
                        compAces++;
                    }

                    // Display the card that was drawn
                    cCard4PictureBox.Image = cardImageList.Images[cHand[3]];

                    // Calculate and display score of computers hand
                    compScore += Score(cHand[3]);
                    int verifiedCompScore = ConvertCompAce(compScore);
                    if (verifiedCompScore < compScore)
                    {
                        compScore = verifiedCompScore;
                    }
                    computerScoreLabel.Text = compScore.ToString();
                }

                // Increase hit number
                hit++;

                // Select next hit button
                hit2Button.Visible = false;
                hit3Button.Visible = true;
            }
        }

        private void hit3Button_Click(object sender, EventArgs e)
        {
            if (hit == 3)
            {
                // Draw users fifth card
                uHand[4] = d.DrawCard();

                // Count Aces in hand
                if (uHand[4] > 47 && uHand[4] < 52)
                {
                    userAces++;
                }

                // Display the card that was drawn
                uCard5PictureBox.Image = cardImageList.Images[uHand[4]];

                // Calculate and display score of users hand
                userScore += Score(uHand[4]);
                int verifiedUserScore = ConvertUserAce(userScore);
                if (verifiedUserScore < userScore)
                {
                    userScore = verifiedUserScore;
                }
                userScoreLabel.Text = userScore.ToString();

                if (compScore < 17)
                {
                    // Draw computers fifth card
                    cHand[4] = d.DrawCard();

                    // Count Aces in hand
                    if (cHand[4] > 47 && cHand[4] < 52)
                    {
                        compAces++;
                    }

                    // Display the card that was drawn
                    cCard5PictureBox.Image = cardImageList.Images[cHand[4]];

                    // Calculate and display score of computers hand
                    compScore += Score(cHand[4]);
                    int verifiedCompScore = ConvertCompAce(compScore);
                    if (verifiedCompScore < compScore)
                    {
                        compScore = verifiedCompScore;
                    }
                    computerScoreLabel.Text = compScore.ToString();
                }

                // Increase hit number
                hit++;
            }
        }

    }
}
